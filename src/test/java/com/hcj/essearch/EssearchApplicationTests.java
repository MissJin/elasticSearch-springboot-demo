package com.hcj.essearch;

import com.alibaba.fastjson.JSON;
import com.hcj.essearch.utils.IdWorker;
import com.hcj.essearch.document.ProductDocument;
import com.hcj.essearch.document.ProductDocumentBuilder;
import com.hcj.essearch.page.Page;
import com.hcj.essearch.service.EsSearchService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EssearchApplicationTests {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private EsSearchService esSearchService;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    private static final String idStr1 = IdWorker.getIdStr();
    private static final String idStr2 = IdWorker.getIdStr();
    private static final String idStr3 = IdWorker.getIdStr();

    @Test
    public void a_save() {
        log.info("【创建索引前的数据条数】：{}", esSearchService.getAll().size());
        ProductDocument productDocumentA = new ProductDocument()
                .setId(idStr1)
                .setProductName("aa产品")
                .setProductDesc("aa产品描述")
                .setCreateTime(new Date()).setUpdateTime(new Date());
        ProductDocument productDocumentB = new ProductDocument()
                .setId(idStr2)
                .setProductName("bb产品")
                .setProductDesc("bb产品描述")
                .setCreateTime(new Date()).setUpdateTime(new Date());
        ProductDocument productDocumentC = new ProductDocument()
                .setId(idStr3)
                .setProductName("cc产品")
                .setProductDesc("cc产品描述")
                .setCreateTime(new Date()).setUpdateTime(new Date());


        esSearchService.save(productDocumentA, productDocumentB, productDocumentC);

        log.info("【创建索引ID】:{},{},{}", productDocumentA.getId(), productDocumentB.getId(), productDocumentC.getId());
        log.info("【创建索引后的数据条数】：{}", esSearchService.getAll().size());
    }

    @Test
    public void b_getAll() {
        esSearchService.getAll().parallelStream()
                .map(JSON::toJSONString)
                .forEach(System.out::println);
    }


    @Test
    public void c_getById() {
        log.info("【根据ID查询内容】：{}", JSON.toJSONString(esSearchService.getById(idStr1)));
    }

    @Test
    public void d_query() {
        log.info("【根据关键字搜索内容】：{}", JSON.toJSONString(esSearchService.query("aa", ProductDocument.class)));
    }

    @Test
    public void e_queryHit() {

        String keyword = "aa";
        String indexName = "orders";

        List<Map<String, Object>> searchHits = esSearchService.queryHit(keyword, indexName, "productName", "productDesc");
        log.info("【根据关键字搜索内容，命中部分高亮，返回内容】：{}", JSON.toJSONString(searchHits));
    }

    @Test
    public void f_queryHitByPage() {

        String keyword = "aa";
        String indexName = "orders";

        Page<Map<String, Object>> searchHits = esSearchService.queryHitByPage(1, 1, keyword, indexName, "productName", "productDesc");
        log.info("【分页查询，根据关键字搜索内容，命中部分高亮，返回内容】：{}", JSON.toJSONString(searchHits));
    }

    @Test
    public void g_deleteIndex() {
        log.info("【删除索引库】");
        if(elasticsearchTemplate.indexExists("orders")){
            esSearchService.deleteIndex("orders");
        }
    }

    @Test
    public void h_deleteAll() {
        if (elasticsearchTemplate.indexExists("orders") && esSearchService.getAll().size() > 0) {
            // 如果索引存在，并且索引内有值的情况下做删除操作【目前库中只有一个索引 orders】
            esSearchService.deleteAll();
        }
    }

}

