package com.hcj.essearch.document;

import com.hcj.essearch.utils.IdWorker;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.Mapping;

import java.io.Serializable;
import java.util.Date;

/**
 * 产品实体
 */
@Document(indexName = "orders", type = "product")
@Mapping(mappingPath = "productIndex.json") // 解决IK分词不能使用问题
@Data
@Accessors(chain = true)
public class ProductDocument implements Serializable {

    @Id
    private String id;
    //@Field(analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String productName;
    //@Field(analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String productDesc;

    private Date createTime;

    private Date updateTime;

    public ProductDocument(){
        super();
    }

    public ProductDocument(String id, String productName, String productDesc) {
        this.id = id;
        this.productName = productName;
        this.productDesc = productDesc;
        this.createTime = new Date();
        this.updateTime = new Date();
    }

    public ProductDocument(String productName, String productDesc) {
        this.id = IdWorker.getIdStr();
        this.productName = productName;
        this.productDesc = productDesc;
        this.createTime = new Date();
        this.updateTime = new Date();
    }

    public ProductDocument(String id, String productName, String productDesc, Date createTime, Date updateTime) {
        this.id = id;
        this.productName = productName;
        this.productDesc = productDesc;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }


}
