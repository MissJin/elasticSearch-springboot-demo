package com.hcj.essearch.service;

import com.hcj.essearch.document.ProductDocument;

import java.util.List;

/**
 *
 */
public interface EsSearchService extends BaseSearchService<ProductDocument> {
    /**
     * 保存
     */
    void save(ProductDocument... productDocuments);

    /**
     * 删除
     *
     * @param id
     */
    void delete(String id);

    /**
     * 清空索引
     */
    void deleteAll();

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    ProductDocument getById(String id);

    /**
     * 查询全部
     *
     * @return
     */
    List<ProductDocument> getAll();
}
